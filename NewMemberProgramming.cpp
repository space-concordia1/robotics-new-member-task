#include <iostream>

using namespace std;

double vbattRead()
{
    double vsense, vbatt;
    vsense = (analogRead() * 3.3)/1023;
    /* I hope I am applying voltage division correctly here.
    Upon encountering the resistor, there should be a voltage drop of:
    vdrop = (10 k/(10 k + 2 k))* vbatt = 5vbatt / 6
    Thus, vsense = vbatt - vdrop = vbatt (1-5/6)=vbatt/6
    Then vbatt = 6*vsense   */
    vbatt = 6 * vsense;
    cout << "The battery's voltage is: " << vbatt << " V\n";
    if (vbatt > 16.8 || vbatt <12)
    {
        cout << "This is a warning!";
    }
    return vbatt;
}

int main()
{
    vbattRead();

    return 0;
}
